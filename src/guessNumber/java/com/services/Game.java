package guessNumber.java.com.services;

import java.util.Random;
import java.util.Scanner;

public class Game {

    private int number;


    private int min=1;
    private int max=100;
    private int attempt=5;


    Random rnd;

    Scanner in;

    public Game(Random rnd, Scanner in) {
        this.rnd = rnd;
        this.in = in;
    }

    public int getAttempt() {
        return attempt;
    }

    public void startGame(){
        System.out.println("Привет, я загадал число от min до max вашего диапазона. Попробуй угадать его за x попыток!”");
        System.out.println("\nДля начала выбери диапозон и количество попыток");
        getRange();
    }
    int warmCold =0;
    public void guessTheNumber()
    {
        int indexOfTry=1;

        number = min + rnd.nextInt(max - min + 1);
        while (indexOfTry<=attempt)
        {
            System.out.println("Я загадал...");
            String userNumber  = in.next();
            if (userNumber.equals("exit"))
            {
                System.exit(0);
            }
            else if (number == Integer.parseInt(userNumber))
            {
                System.out.println("Поздравляю! Ты угадал задуманное число за "+indexOfTry+" попыток. Попробуй сыграть еще раз!");
                getRange();
            }
            else if(indexOfTry==1)
            {
                System.out.println("Не угадал, попробуй еще раз!");
                warmCold = Math.abs(number-Integer.parseInt(userNumber));
            }
            else if(indexOfTry>=2){
                if(warmCold>(number-Integer.parseInt(userNumber)))
                {
                    System.out.println("Не угадал, но теплее!!! Осталось " + (attempt-indexOfTry)+ " попыток");
                }
                else
                {
                    System.out.println("Не угадал, холоднее… Осталось " + (attempt-indexOfTry)+ " попыток");
                }
                warmCold = Math.abs(number-Integer.parseInt(userNumber));

            }
            indexOfTry++;
        }
        System.out.println("Количество попыток исчерпано :( \nЯ загадал число "+number+" Начни игру заново!");
        getRange();

    }
    public void getRange (){
        System.out.println("Введите значения диапозона, в котором компьютер загадает число (от 1 до 200)");
        System.out.println("min = ");
        int userMin = in.nextInt();;
        System.out.println("max = ");
        int userMax = in.nextInt();;
        if (userMin>=1 && userMax>userMin && userMax<=200){
            min = userMin;
            max = userMax;
            getTry();
        }
        else {
            System.out.println("Вы ввели неправильные значения max и min! Ваши значения должны входить в диапозон от 1 до 200. Попробуйте снова!");
            getRange();
        }


    }
    public void getTry(){
        System.out.println("Введите количесво попыток (от 1 до 15)");
        int userAttempt = in.nextInt();;
        if (userAttempt>=1 && userAttempt<=15)
        {
            attempt = userAttempt;
            guessTheNumber();
        }
        else{
            System.out.println("Вы ввели неправильное количество попыток. Минимальное количество попыток - 1, максимальное - 15");
            getTry();
        }
    }



}
