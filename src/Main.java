import guessNumber.java.com.services.Game;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random rnd = new Random();

        Scanner in = new Scanner(System.in);
        Game game = new Game(rnd,in);
        game.startGame();
    }
}
