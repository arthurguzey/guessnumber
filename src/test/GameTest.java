package test;

import guessNumber.java.com.services.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;


import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GameTest {
    Random rnd = Mockito.mock(Random.class);

    Scanner in = Mockito.mock(Scanner.class);
    Game cut = new Game(rnd, in);





    static List<Arguments> StartGameTestArgs() {
        return List.of(
                Arguments.arguments(3, 3),
                Arguments.arguments(10, 10)
        );
    }
    @ParameterizedTest
    @MethodSource("StartGameTestArgs")
    void StartGameTest(int userAttempt, int expected){
        Mockito.when(in.nextInt()).thenReturn(userAttempt);
        int actual = cut.getAttempt();
        Assertions.assertEquals(expected, actual);
    }


}
